package com.qulix.dbo;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JVar;

/**
 * @author PotenkoDV
 */
public abstract class Generator {

    protected boolean isStandardReturnType(Method method) {
        Class returnType = method.getReturnType();
        return returnType.isPrimitive() || returnType.getPackage().getName().startsWith("java.") || returnType.isEnum();
    }

    protected List<Method> findGetterMethods(Class generatedClass) {
        return Arrays.stream(generatedClass.getMethods()).filter(method1 -> method1.getName().startsWith("get") && !method1.getName().equals("getClass")).collect(Collectors.toList());
    }

    protected List<Method> findSetterMethods(Class dtoClass) {
        return Arrays.stream(dtoClass.getMethods()).filter(method -> method.getName().startsWith("set")).collect(Collectors.toList());
    }

    protected JInvocation createGetterInvoke(Method getMethod, JVar generatedStubInstanceIs) {
        if (getMethod.getReturnType().isEnum()) {
            return generatedStubInstanceIs.invoke(getMethod.getName()).invoke("name");
        } else {
            return generatedStubInstanceIs.invoke(getMethod.getName());
        }
    }

    protected boolean isStandardParameterType(Method method) {
        Class parameterType = method.getParameterTypes()[0];
        return parameterType.isPrimitive() || parameterType.getPackage().getName().startsWith("java.");
    }

    protected boolean isMethodExists(Class anotherClass, Method methodForCheck) {
        return Arrays.stream(anotherClass.getMethods()).anyMatch(method -> method.getName().equals(methodForCheck.getName()));
    }
}
