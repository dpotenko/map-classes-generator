package com.qulix.dbo;

import java.beans.Introspector;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import com.sun.codemodel.JClass;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JVar;

/**
 * @author PotenkoDV
 */
public class MatcherGenerator extends Generator {

    private JCodeModel codeModel;
    private Class dtoComparedClass;

    public JClass findMatcher(Class returnType, JCodeModel codeModel) throws JClassAlreadyExistsException, ClassNotFoundException, IOException {
        try {
            return codeModel.ref(Class.forName("com.qulix.dbo.typemap.converters.support." + returnType.getSimpleName() + "Matcher"));
        }
        catch (ClassNotFoundException e) {
            return generateMatcher(returnType);
        }
    }

    public JClass generateMatcher(Class isComparedClass) throws JClassAlreadyExistsException, ClassNotFoundException, IOException {
        codeModel = new JCodeModel();

        dtoComparedClass = Class.forName("com.qulix.dbo.bankwsapi.sei." + isComparedClass.getSimpleName() + "Dto");

        JDefinedClass generatedMather = generateClassDeclaration(isComparedClass);

        JFieldVar dtoComparedField = generatedMather.field(JMod.PRIVATE, dtoComparedClass, Introspector.decapitalize(dtoComparedClass.getSimpleName()));

        createConstructor(generatedMather, dtoComparedClass);

        createMatchesMethod(generatedMather, isComparedClass, dtoComparedField);

        createDescribeToMethod(generatedMather, dtoComparedField);

        createFabricMethod(generatedMather, isComparedClass, dtoComparedClass);

        codeModel.build(new File("com.qulix.dbo.bankwsapi\\src\\test\\java"));
        return generatedMather;
    }

    private JDefinedClass generateClassDeclaration(Class isComparedClass) throws ClassNotFoundException, JClassAlreadyExistsException {
        JDefinedClass generatedMather = codeModel._class("com.qulix.dbo.typemap.converters.support." + isComparedClass.getSimpleName() + "Matcher");
        generatedMather._extends(codeModel.ref(BaseMatcher.class).narrow(isComparedClass));
        generatedMather.javadoc().append("Матчер ").append(codeModel.ref(dtoComparedClass)).append(" и ").append(codeModel.ref(isComparedClass)).add("\n\n@author Q-PDV");
        return generatedMather;
    }

    private JExpression equalsStatement(List<Method> getMethods, JFieldVar dtoInstance, JVar isInstance) throws JClassAlreadyExistsException, ClassNotFoundException, IOException {
        JExpression resultExpression = null;
        for (Method getMethod : getMethods) {
            if (!isMethodExists(dtoComparedClass, getMethod)) {
                continue;
            }
            resultExpression = appendPartOfEqualsStatement(getMethod, resultExpression, isInstance, dtoInstance);
        }
        return resultExpression;
    }

    private JExpression appendPartOfEqualsStatement(Method getMethod, JExpression resultExpression, JVar isInstance, JVar dtoInstance) throws JClassAlreadyExistsException, ClassNotFoundException, IOException {
        Class<?> returnType = getMethod.getReturnType();
        JClass jObjects = codeModel.ref(Objects.class);
        JInvocation isGetInvocation = createGetterInvoke(getMethod, isInstance);
        JInvocation dtoGetInvocation = createGetterInvoke(getMethod, dtoInstance);
        if (isStandardReturnType(getMethod)) {
            return appendExpressionAND(resultExpression, jObjects.staticInvoke("equals")
                .arg(dtoGetInvocation)
                .arg(isGetInvocation));
        } else {
            MatcherGenerator matcherGenerator = new MatcherGenerator();
            JClass matcher = matcherGenerator.findMatcher(returnType, codeModel);
            return appendExpressionAND(resultExpression, matcher
                .staticInvoke("same" + StringUtils.capitalize(returnType.getSimpleName()))
                .arg(dtoGetInvocation).invoke("matches")
                .arg(isGetInvocation));
        }
    }

    private JExpression appendExpressionAND(JExpression resultExpression, JExpression appendedExpression) {
        if (resultExpression == null) {
            return appendedExpression;
        } else {
            return resultExpression.cand(appendedExpression);
        }
    }

    private void createConstructor(JDefinedClass generatedClass, Class paramClass) {
        JMethod constructor = generatedClass.constructor(JMod.PUBLIC);
        JVar constructorParameter = constructor.param(paramClass, Introspector.decapitalize(paramClass.getSimpleName()));
        constructor.body().assign(JExpr.refthis(Introspector.decapitalize(paramClass.getSimpleName())), constructorParameter);

    }

    private void createMatchesMethod(JDefinedClass generatedClass, Class comparedParamIsClass, JFieldVar comparedFieldDto) throws JClassAlreadyExistsException, ClassNotFoundException, IOException {
        JMethod method = generatedClass.method(JMod.PUBLIC, boolean.class, "matches");
        method.annotate(Override.class);
        JVar item = method.param(Object.class, "item");
        method.body()._if(item.eq(JExpr._null()))._then()._return(comparedFieldDto.eq(JExpr._null()));

        JClass jComparedParamIsClass = codeModel.ref(comparedParamIsClass);
        JVar isInstance = method.body().decl(jComparedParamIsClass, Introspector.decapitalize(comparedParamIsClass.getSimpleName()), JExpr.cast(jComparedParamIsClass, item));

        List<Method> getMethods = findGetterMethods(comparedParamIsClass);
        method.body()._return(equalsStatement(getMethods, comparedFieldDto, isInstance));
    }

    private void createDescribeToMethod(JDefinedClass generatedClass, JFieldVar dtoField) {
        JMethod describe = generatedClass.method(JMod.PUBLIC, void.class, "describeTo");
        describe.annotate(Override.class);
        JVar paramVar = describe.param(Description.class, "description");

        describe.body().add(paramVar.invoke("appendText").arg("same as ").invoke("appendValue").arg(dtoField));
    }

    private void createFabricMethod(JDefinedClass generatedClass, Class isComparedClass, Class dtoComparedClass) {
        JMethod fabricMethod = generatedClass.method(JMod.PUBLIC + JMod.STATIC, generatedClass, "same" + isComparedClass.getSimpleName());
        JVar sameInvoiceServiceMetersParam = fabricMethod.param(dtoComparedClass, Introspector.decapitalize(dtoComparedClass.getSimpleName()));
        fabricMethod.body()._return(JExpr._new(generatedClass).arg(sameInvoiceServiceMetersParam));
    }


}
