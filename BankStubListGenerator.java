package com.qulix.dbo.bankstub;

import java.io.File;
import java.io.IOException;

import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import com.qulix.dbo.bankstub.bankfacade.util.Unmarshaller;
import com.qulix.dbo.bankwsapi.sei.BankItemDto;
import com.qulix.dbo.bankwsapi.sei.GetListResponse;
import com.qulix.dbo.operation.item.BankItem;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpressionImpl;
import com.sun.codemodel.JFormatter;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;

/**
 * @author Q-PDV
 */
public class BankStubListGenerator {
    private static JCodeModel codeModel;

    private static JDefinedClass fabricClass;

    private static final Unmarshaller unmarshaller = new Unmarshaller();
    private static final PathMatchingResourcePatternResolver pathResolver = new PathMatchingResourcePatternResolver();


    public static void main(String[] args) throws JClassAlreadyExistsException, IOException {

        GetListResponse getListResponse = (GetListResponse) unmarshaller.tryUnmarshalResource(pathResolver.getResource("responses/operation/List_banks.xml"));

        codeModel = new JCodeModel();

        fabricClass = codeModel._class("com.qulix.dbo.bankstub.Fabric");

        JMethod createMethod = fabricClass.method(JMod.PUBLIC + JMod.STATIC, BankItem.class, "create" + createIsClassNameFromDto(BankItem.class));
        getListResponse.getItem().forEach(it -> createBanks(createMethod, codeModel, BankItem.class, (BankItemDto) it));

        JExpr.invoke(createMethod);

        codeModel.build(new File("com.qulix.dbo.bankstub\\src\\test\\java"));
    }

    private static void createBanks(JMethod createMethod, JCodeModel codeModel, Class dtoClass, BankItemDto item) {


        JInvocation init = JExpr._new(codeModel.ref(dtoClass));

        init.arg(item.getId());
        init.arg(new JStringLiteral(item.getName()));
        init.arg(item.getCode());

        createMethod.body()
            .add(init);

    }


    private static class JStringLiteral extends JExpressionImpl {
        public final String str;


        JStringLiteral(String what) {
            this.str = what;

        }


        public void generate(JFormatter f) {
            String stringBuilder = "\"" +
                str +
                '"';
            f.p(stringBuilder);
        }
    }

    private static String createIsClassNameFromDto(Class dtoClass) {
        return dtoClass.getSimpleName().substring(0, dtoClass.getSimpleName().length() - 3);
    }


}
