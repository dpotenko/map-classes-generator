package generator;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JVar;

/**
 * @author PotenkoDV
 */
public abstract class Generator {

    protected boolean isStandardReturnType(Method method) {
        Class returnType = method.getReturnType();
        return returnType.isPrimitive() || returnType.getPackage().getName().startsWith("java.") || returnType.isEnum();
    }

    protected boolean isCollectionReturnType(Method method) {
        return Collection.class.isAssignableFrom(method.getReturnType());
    }

    protected boolean isArrayReturnType(Method method) {
        return method.getReturnType().getName().startsWith("[L");
    }

    protected List<Method> findGetterMethods(Class generatedClass) {
        return Arrays.stream(generatedClass.getMethods()).filter(method1 -> method1.getName().startsWith("get") && !method1.getName().equals("getClass")).collect(Collectors.toList());
    }

    protected List<Method> findSetterMethods(Class dtoClass) {
        return Arrays.stream(dtoClass.getMethods()).filter(method -> method.getName().startsWith("set")).collect(Collectors.toList());
    }

    protected JInvocation createGetterInvoke(Method getMethod, JVar generatedStubInstanceIs) {
        if (getMethod.getReturnType().isEnum()) {
            return generatedStubInstanceIs.invoke(getMethod.getName()).invoke("name");
        } else {
            return generatedStubInstanceIs.invoke(getMethod.getName());
        }
    }

    protected boolean isStandardParameterType(Method method) {
        Class parameterType = method.getParameterTypes()[0];
        return isStandardParameterType(parameterType);
    }

    protected boolean isStandardParameterType(Class clazz) {
        return clazz.isPrimitive() || isArrayReturnType(clazz) || clazz.getPackage().getName().startsWith("java.");
    }

    private boolean isArrayReturnType(Class clazz) {
        return clazz.getName().startsWith("[L");
    }


    protected boolean isMethodExists(Class anotherClass, Method methodForCheck) {
        return Arrays.stream(anotherClass.getMethods()).anyMatch(method -> method.getName().equals(methodForCheck.getName()));
    }

    protected String simpleIsName(Class clazz) {
        if (clazz.getSimpleName().endsWith("Dto") || clazz.getSimpleName().endsWith("Mto")) {
            return clazz.getSimpleName().substring(0, clazz.getSimpleName().length() - 3);
        } else {
            return clazz.getSimpleName();
        }
    }
}
