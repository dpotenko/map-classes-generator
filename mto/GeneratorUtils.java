package generator;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import static java.beans.Introspector.decapitalize;

import javax.xml.bind.annotation.XmlType;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.core.annotation.AnnotationUtils;

import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JVar;

/**
 * @author Q-PDV
 */
public abstract class GeneratorUtils {

    public static boolean isStandardReturnType(Method method) {
        Class returnType = method.getReturnType();
        return returnType.isPrimitive() || returnType.getPackage().getName().startsWith("java.") || returnType.isEnum();
    }

    public static boolean isCollectionReturnType(Method method) {
        return Collection.class.isAssignableFrom(method.getReturnType());
    }

    public static boolean isArrayReturnType(Method method) {
        return isArrayReturnType(method.getReturnType());
    }

    public static List<Method> findGetterMethods(Class generatedClass) {
        return Arrays.stream(generatedClass.getMethods()).filter(method1 -> method1.getName().startsWith("get") &&
            !method1.getName().equals("getClass")).collect(Collectors.toList());
    }

    public static List<Method> findSetterMethods(Class dtoClass) {
        return Arrays.stream(dtoClass.getMethods()).filter(method -> method.getName().startsWith("set"))
            .collect(Collectors.toList());
    }

    public static JInvocation createGetterInvoke(Method getMethod, JVar generatedStubInstanceIs) {
        if (getMethod.getReturnType().isEnum()) {
            return generatedStubInstanceIs.invoke(getMethod.getName()).invoke("name");
        } else {
            return generatedStubInstanceIs.invoke(getMethod.getName());
        }
    }

    public static boolean isStandardParameterType(Method method) {
        Class parameterType = method.getParameterTypes()[0];
        return isStandardParameterType(parameterType);
    }

    public static boolean isBigDecimalReturnType(Method method) {
        return BigDecimal.class.isAssignableFrom(method.getReturnType());
    }

    public static boolean isStandardParameterType(Class clazz) {
        return clazz.isPrimitive() || isArrayReturnType(clazz) || clazz.getPackage().getName().startsWith("java.");
    }

    private static boolean isArrayReturnType(Class clazz) {
        return clazz.getName().startsWith("[");
    }


    public static boolean isMethodExists(Class anotherClass, Method methodForCheck) {
        return Arrays.stream(anotherClass.getMethods()).anyMatch(method -> method.getName().equals(methodForCheck.getName()));
    }

    public static String simpleIsName(Class clazz) {
        if (clazz.getSimpleName().endsWith("Dto") || clazz.getSimpleName().endsWith("Mto")) {
            return clazz.getSimpleName().substring(0, clazz.getSimpleName().length() - 3);
        } else {
            return clazz.getSimpleName();
        }
    }

    public static String fieldNameByGetSetMethod(Method method) {
        return decapitalize(method.getName().substring(3));
    }

    public static Method getMethodByField(Class clazz, String fieldName) {
        return findGetterMethods(clazz).stream()
            .filter(getMethod -> getMethod.getName().substring(3).equals(StringUtils.capitalize(fieldName)))
            .findFirst().orElse(null);
    }

    public static List<Method> sortSetGetMethods(List<Method> setters, Class mtoClass) throws ClassNotFoundException {
        Class dtoClass = Class.forName("com.qulix.dbo.bankwsapi.sei." + simpleIsName(mtoClass) + "Dto");

        Annotation xmlType = AnnotationUtils.getAnnotation(dtoClass, XmlType.class);


        List<String> propOrder = Arrays.asList((String[]) AnnotationUtils.getAnnotationAttributes(xmlType).get("propOrder"));

        Class dtoSuperClass = dtoClass.getSuperclass();

        while (!dtoSuperClass.equals(Object.class)) {
            XmlType xmlTypeSuper = AnnotationUtils.getAnnotation(dtoSuperClass, XmlType.class);
            if (xmlTypeSuper == null) {
                dtoSuperClass = dtoSuperClass.getSuperclass();
                continue;
            }
            propOrder = ListUtils.union(Arrays.asList((String[]) AnnotationUtils.getAnnotationAttributes(xmlTypeSuper).get("propOrder")), propOrder);
            dtoSuperClass = dtoSuperClass.getSuperclass();
        }

        Map<String, Method> setMethodNameToMethod = setters.stream().
            collect(Collectors.toMap(GeneratorUtils::fieldNameByGetSetMethod, setter -> setter));

        return propOrder.stream().filter(setMethodNameToMethod::containsKey).map(setMethodNameToMethod::get)
            .collect(Collectors.toList());
    }

}
