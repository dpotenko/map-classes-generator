package generator;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import static java.beans.Introspector.decapitalize;

import org.hamcrest.Matchers;
import org.junit.Assert;

import com.sun.codemodel.JClass;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JDocComment;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JVar;
import com.ukrsibbank.client.endpoint.StubGenerator;
import com.ukrsibbank.client.protocol.product.loan.LoanMto;

import static generator.GeneratorUtils.createGetterInvoke;
import static generator.GeneratorUtils.findGetterMethods;
import static generator.GeneratorUtils.getMethodByField;
import static generator.GeneratorUtils.isArrayReturnType;
import static generator.GeneratorUtils.isBigDecimalReturnType;
import static generator.GeneratorUtils.isCollectionReturnType;
import static generator.GeneratorUtils.isStandardReturnType;
import static generator.GeneratorUtils.simpleIsName;
import static generator.GeneratorUtils.sortSetGetMethods;

/**
 * @author PotenkoDV
 */
public class TestGenerator {

    private JCodeModel codeModel = new JCodeModel();
    private MatcherGenerator matcherGenerator = new MatcherGenerator();

    public static void main(String[] args) throws JClassAlreadyExistsException, IOException, ClassNotFoundException {

        TestGenerator testGenerator = new TestGenerator();
        testGenerator.generateTestClass(LoanMto.class, false);

       /* MatcherGenerator matcherGenerator = new MatcherGenerator();
        matcherGenerator.generateMatcher(Transaction.class);*/
    }


    public void generateTestClass(Class mtoClass, boolean isTwoWay) throws IOException, JClassAlreadyExistsException, ClassNotFoundException {
        Class isClass = Class.forName("com.qulix.dbo.bankapi.bank." + simpleIsName(mtoClass));
        JDefinedClass generatedTestClass = generateClassDeclaration(mtoClass, isClass, isTwoWay);
        generateTestMethod(generatedTestClass, isClass, mtoClass, true);
        if (isTwoWay) {
            generateTestMethod(generatedTestClass, isClass, mtoClass, false);
        }
        codeModel.build(new File("com.ukrsibbank.client.endpoint\\src\\test\\java"));
    }

    private JDefinedClass generateClassDeclaration(Class mtoClass, Class isClass, boolean isTwoWay) throws JClassAlreadyExistsException {
        JDefinedClass generatedTestClass = codeModel._class("com.ukrsibbank.client.endpoint.typemap.converters.Mto" + simpleIsName(mtoClass) + "ConverterTest");
        JDocComment comment = generatedTestClass.javadoc().append("Тесты для конвертера ").append(codeModel.ref(isClass)).append(" в ").append(codeModel.ref(mtoClass));
        if (isTwoWay) {
            comment.append("и обратно");
        }
        comment.add("\n@author Q-PDV");
        return generatedTestClass;
    }

    private JVar initStubInstance(JMethod testMethod, Class generatedStubClass, JExpression initExpression, boolean isMto) {
        JClass jIsClass = codeModel.ref(generatedStubClass);
        return testMethod.body().decl(jIsClass, isMto ? "mtoObj" : "isObj", initExpression);
    }

    private void generateTestMethod(JDefinedClass generatedTestClass, Class isClass, Class mtoClass, boolean isDirect) throws JClassAlreadyExistsException, IOException, ClassNotFoundException {
        String methodName;
        if (isDirect) {
            methodName = "testConvert";
        } else {
            methodName = "testConvertBack";
        }
        JMethod testMethod = generatedTestClass.method(JMod.PUBLIC, void.class, methodName);
        testMethod.annotate(org.junit.Test.class);

        JClass stubGenerator = codeModel.ref(StubGenerator.class);

        JVar generatedStubInstanceMto;
        JVar generatedStubInstanceIs;

        JClass converterClass = codeModel.ref(Class.forName("com.ukrsibbank.client.endpoint.typemap.converters.Mto" + isClass.getSimpleName() + "Converter"));

        if (isDirect) {
            JInvocation convertInvocation = JExpr._new(converterClass).invoke("convert");

            generatedStubInstanceIs = initStubInstance(testMethod, isClass, stubGenerator.staticInvoke(decapitalize(isClass.getSimpleName())), false);
            generatedStubInstanceMto = initStubInstance(testMethod, mtoClass, convertInvocation.arg(generatedStubInstanceIs), true);
        } else {
            JInvocation convertInvocation = JExpr._new(converterClass).invoke("convert");
            JInvocation convertBackInvocation = JExpr._new(converterClass).invoke("convertBack");

            generatedStubInstanceMto = initStubInstance(testMethod, mtoClass, convertInvocation.arg(stubGenerator.staticInvoke(decapitalize(isClass.getSimpleName()))), true);
            generatedStubInstanceIs = initStubInstance(testMethod, isClass, convertBackInvocation.arg(generatedStubInstanceMto), false);
        }

        testMethod.body().directStatement(" ");

        JClass jAssert = codeModel.ref(Assert.class);

        List<Method> methods = findGetterMethods(mtoClass);

        methods = sortSetGetMethods(methods, mtoClass);

        JClass jMatcher = codeModel.ref(Matchers.class);
        for (Method getMethod : methods) {
           /* if (!isMethodExists(dtoClass, getMethod)) {
                continue;
            }*/
            JInvocation assertInvoke = testMethod.body().staticInvoke(jAssert, "assertThat");

            JInvocation getterInvokeMto = createGetterInvoke(getMethod, generatedStubInstanceMto);
            JInvocation getterInvokeIs = createGetterInvoke(getMethod, generatedStubInstanceIs);

            Method isGetMethod = getMethodByField(isClass, decapitalize(getMethod.getName().substring(3)));

            if (isArrayReturnType(getMethod)) {
                assertInvoke.arg(getterInvokeMto.ref("length"));
                assertInvoke.arg(jMatcher.staticInvoke("is").arg(getterInvokeIs.invoke("size")));
            } else if (isCollectionReturnType(getMethod)) {
                assertInvoke.arg(getterInvokeMto.invoke("size"));
                assertInvoke.arg(jMatcher.staticInvoke("is").arg(getterInvokeIs.invoke("size")));
            } else if (isGetMethod != null && isBigDecimalReturnType(isGetMethod)) {
                assertInvoke.arg(getterInvokeMto);
                assertInvoke.arg(jMatcher.staticInvoke("is").arg(getterInvokeIs.invoke("doubleValue")));
            } else if (isStandardReturnType(getMethod)) {
                assertInvoke.arg(getterInvokeMto);
                assertInvoke.arg(jMatcher.staticInvoke("is").arg(getterInvokeIs));
            } else {
                assertInvoke.arg(getterInvokeMto);
                JClass matcher = matcherGenerator.findMatcher(getMethod.getReturnType(), codeModel);
                assertInvoke.arg(matcher.staticInvoke("same" + simpleIsName(getMethod.getReturnType())).arg(getterInvokeIs));
            }
        }
    }


}
