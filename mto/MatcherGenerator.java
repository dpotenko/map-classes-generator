package generator;

import java.beans.Introspector;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import com.sun.codemodel.JClass;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JVar;

import static generator.GeneratorUtils.createGetterInvoke;
import static generator.GeneratorUtils.findGetterMethods;
import static generator.GeneratorUtils.isMethodExists;
import static generator.GeneratorUtils.isStandardReturnType;
import static generator.GeneratorUtils.simpleIsName;

/**
 * @author PotenkoDV
 */
public class MatcherGenerator {

    private JCodeModel codeModel;
    private Class isComparedClass;

    public JClass findMatcher(Class returnType, JCodeModel codeModel) throws JClassAlreadyExistsException, ClassNotFoundException, IOException {
        try {
            return codeModel.ref(Class.forName("com.ukrsibbank.client.endpoint.typemap.converters.support." + simpleIsName(returnType) + "Matcher"));
        }
        catch (ClassNotFoundException e) {
            return generateMatcher(returnType);
        }
    }

    public JClass generateMatcher(Class mtoComparedClass) throws JClassAlreadyExistsException, ClassNotFoundException, IOException {
        codeModel = new JCodeModel();

        isComparedClass = Class.forName("com.qulix.dbo.bankapi.bank." + simpleIsName(mtoComparedClass));

        JDefinedClass generatedMather = generateClassDeclaration(mtoComparedClass);

        JFieldVar isComparedField = generatedMather.field(JMod.PRIVATE, isComparedClass, Introspector.decapitalize(isComparedClass.getSimpleName()));

        createConstructor(generatedMather, isComparedClass);

        createMatchesMethod(generatedMather, mtoComparedClass, isComparedField);

        createDescribeToMethod(generatedMather, isComparedField);

        createFabricMethod(generatedMather, mtoComparedClass, isComparedClass);

        codeModel.build(new File("com.ukrsibbank.client.endpoint\\src\\test\\java"));

        return generatedMather;
    }

    private JDefinedClass generateClassDeclaration(Class mtoComparedClass) throws ClassNotFoundException, JClassAlreadyExistsException {
        JDefinedClass generatedMather = codeModel._class("com.ukrsibbank.client.endpoint.typemap.converters.support." + simpleIsName(mtoComparedClass) + "Matcher");
        generatedMather._extends(codeModel.ref(BaseMatcher.class).narrow(mtoComparedClass));
        generatedMather.javadoc().append("Матчер ").append(codeModel.ref(this.isComparedClass)).append(" и ").append(codeModel.ref(mtoComparedClass)).add("\n\n@author Q-PDV");
        return generatedMather;
    }

    private JExpression equalsStatement(List<Method> getMethods, JFieldVar isInstance, JVar mtoInstance) throws JClassAlreadyExistsException, ClassNotFoundException, IOException {
        JExpression resultExpression = null;
        for (Method getMethod : getMethods) {
            if (!isMethodExists(isComparedClass, getMethod)) {
                continue;
            }
            resultExpression = appendPartOfEqualsStatement(getMethod, resultExpression, mtoInstance, isInstance);
        }
        return resultExpression;
    }

    private JExpression appendPartOfEqualsStatement(Method getMethod, JExpression resultExpression, JVar mtoInstance, JVar isInstance) throws JClassAlreadyExistsException, ClassNotFoundException, IOException {
        Class<?> returnType = getMethod.getReturnType();
        JClass jObjects = codeModel.ref(Objects.class);
        JInvocation mtoGetInvocation = createGetterInvoke(getMethod, mtoInstance);
        JInvocation isGetInvocation = createGetterInvoke(getMethod, isInstance);
        if (isStandardReturnType(getMethod)) {
            return appendExpressionAND(resultExpression, jObjects.staticInvoke("equals")
                .arg(isGetInvocation)
                .arg(mtoGetInvocation));
        } else {
            MatcherGenerator matcherGenerator = new MatcherGenerator();
            JClass matcher = matcherGenerator.findMatcher(returnType, codeModel);
            return appendExpressionAND(resultExpression, matcher
                .staticInvoke("same" + simpleIsName(returnType))
                .arg(isGetInvocation).invoke("matches")
                .arg(mtoGetInvocation));
        }
    }

    private JExpression appendExpressionAND(JExpression resultExpression, JExpression appendedExpression) {
        if (resultExpression == null) {
            return appendedExpression;
        } else {
            return resultExpression.cand(appendedExpression);
        }
    }

    private void createConstructor(JDefinedClass generatedClass, Class paramClass) {
        JMethod constructor = generatedClass.constructor(JMod.PUBLIC);
        JVar constructorParameter = constructor.param(paramClass, Introspector.decapitalize(paramClass.getSimpleName()));
        constructor.body().assign(JExpr.refthis(Introspector.decapitalize(paramClass.getSimpleName())), constructorParameter);

    }

    private void createMatchesMethod(JDefinedClass generatedClass, Class comparedParamMtoClass, JFieldVar comparedFieldIs) throws JClassAlreadyExistsException, ClassNotFoundException, IOException {
        JMethod method = generatedClass.method(JMod.PUBLIC, boolean.class, "matches");
        method.annotate(Override.class);
        JVar item = method.param(Object.class, "item");
        method.body()._if(item.eq(JExpr._null()))._then()._return(comparedFieldIs.eq(JExpr._null()));

        JClass jComparedParamMtoClass = codeModel.ref(comparedParamMtoClass);
        JVar mtoInstance = method.body().decl(jComparedParamMtoClass, Introspector.decapitalize(comparedParamMtoClass.getSimpleName()), JExpr.cast(jComparedParamMtoClass, item));

        List<Method> getMethods = findGetterMethods(comparedParamMtoClass);
        method.body()._return(equalsStatement(getMethods, comparedFieldIs, mtoInstance));
    }

    private void createDescribeToMethod(JDefinedClass generatedClass, JFieldVar isField) {
        JMethod describe = generatedClass.method(JMod.PUBLIC, void.class, "describeTo");
        describe.annotate(Override.class);
        JVar paramVar = describe.param(Description.class, "description");

        describe.body().add(paramVar.invoke("appendText").arg("same as ").invoke("appendValue").arg(isField));
    }

    private void createFabricMethod(JDefinedClass generatedClass, Class mtoComparedClass, Class isComparedClass) {
        JMethod fabricMethod = generatedClass.method(JMod.PUBLIC + JMod.STATIC, generatedClass, "same" + simpleIsName(mtoComparedClass));
        JVar sameInvoiceServiceMetersParam = fabricMethod.param(isComparedClass, Introspector.decapitalize(isComparedClass.getSimpleName()));
        fabricMethod.body()._return(JExpr._new(generatedClass).arg(sameInvoiceServiceMetersParam));
    }


}
