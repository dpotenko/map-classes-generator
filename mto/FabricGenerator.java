package generator;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static java.beans.Introspector.decapitalize;

import com.qulix.dbo.bankapi.bank.AccountRequisites;
import com.qulix.dbo.bankapi.bank.LoanRepaymentAmount;
import com.qulix.dbo.bankapi.bank.Resource;
import com.sun.codemodel.JBlock;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JFieldRef;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JVar;

import static generator.GeneratorUtils.findSetterMethods;
import static generator.GeneratorUtils.isStandardParameterType;

/**
 * @author PotenkoDV
 */
public class FabricGenerator {

    private JCodeModel codeModel;

    private JDefinedClass fabricClass;

    public void generateCreateClassMethod(Class dtoClass) throws JClassAlreadyExistsException, IOException {
        codeModel = new JCodeModel();
        fabricClass = codeModel._class("generator.Fabric");
        recursiveGenerateCreateClassMethod(codeModel, dtoClass);
        codeModel.build(new File("com.ukrsibbank.client.endpoint\\src\\test\\java"));
    }

    public static void main(String[] args) {
        FabricGenerator fabricGenerator = new FabricGenerator();
        try {
            fabricGenerator.generateCreateClassMethod(AccountRequisites.class);
        }
        catch (JClassAlreadyExistsException | IOException e) {
            e.printStackTrace();
        }
    }

    private JExpression recursiveGenerateCreateClassMethod(JCodeModel codeModel, Class clazz) {

        if (clazz.isEnum()) {
            return getEnumFieldRef(clazz);
        }

        JMethod createMethod = fabricClass.method(JMod.PUBLIC + JMod.STATIC, clazz, decapitalize(clazz.getSimpleName()));

        List<Method> setMethods = findSetterMethods(clazz);

        Constructor[] constructors = clazz.getConstructors();

        boolean hasParameters = Stream.of(constructors).anyMatch(constructor -> constructor.getParameterCount() > 0);

        JVar generatedStubInstance;

        if (setMethods.size() == 0 && hasParameters) {
            generatedStubInstance = declareConstructorWithParameters(codeModel, clazz, createMethod, constructors);

        } else {
            generatedStubInstance = createMethod.body()
                .decl(codeModel.ref(clazz), decapitalize(clazz.getSimpleName()), JExpr._new(codeModel.ref(clazz)));

            createMethod.body().directStatement(" ");



            setStubParameters(setMethods, createMethod.body(), generatedStubInstance);
        }

        createMethod.body().directStatement(" ");

        createMethod.body()._return(generatedStubInstance);
        return JExpr.invoke(createMethod);
    }

    private JVar declareConstructorWithParameters(JCodeModel codeModel, Class clazz,
                                                  JMethod createMethod,
                                                  Constructor[] constructors) {
        JVar generatedStubInstance;
        Constructor constructor = Stream.of(constructors)
            .max((constructor1, constructor2) -> new Integer(constructor1.getParameterCount()).compareTo(constructor2.getParameterCount()))
            .get();

        JInvocation init = JExpr._new(codeModel.ref(clazz));

        createMethod.body().directStatement(" ");

        for (Parameter parameter : constructor.getParameters()) {
            Class parameterClass = parameter.getType();

            if (isStandardParameterType(parameterClass)) {
                createPrimitiveInstance(init, parameter.getName(), parameterClass.getSimpleName());
            } else {
                init.arg(recursiveGenerateCreateClassMethod(codeModel, parameterClass));
            }
        }

        generatedStubInstance = createMethod.body()
            .decl(codeModel.ref(clazz), decapitalize(clazz.getSimpleName()), init);
        return generatedStubInstance;
    }

    private void setStubParameters(List<Method> setMethods, JBlock block, JVar generatedStubInstance) {
        for (Method setMethod : setMethods) {
            if (isStandardParameterType(setMethod)) {
                JInvocation invocation = block.invoke(generatedStubInstance, setMethod.getName());
                createPrimitiveInstance(invocation, generateStringParameter(setMethod), setMethod.getParameterTypes()[0].getSimpleName());
            } else {
                block.invoke(generatedStubInstance, setMethod.getName())
                    .arg(recursiveGenerateCreateClassMethod(codeModel, setMethod.getParameterTypes()[0]));
            }
        }
    }

    private void createPrimitiveInstance(JInvocation invocation, String parameterTestStringValue, String simpleName) {
        if (simpleName.equals("String")) {
            invocation.arg("test" + parameterTestStringValue);
        }
        if (simpleName.equals("Date")) {
            invocation.arg(JExpr._new(codeModel.ref(Date.class)));
        }
        if (simpleName.equals("BigDecimal")) {
            invocation.arg(JExpr._new(codeModel.ref(BigDecimal.class)).arg(JExpr.lit(Integer.toString(ThreadLocalRandom.current().nextInt(0, 1000)))));
        }
        if (simpleName.equals("Boolean") || simpleName.equals("boolean")) {
            invocation.arg(JExpr.lit(new Random().nextBoolean()));
        }
        if (simpleName.equals("int")) {
            invocation.arg(JExpr.lit(ThreadLocalRandom.current().nextInt(0, 1000)));
        }
    }

    private JFieldRef getEnumFieldRef(Class dtoClass) {
        List<Field> enumFieldList = Arrays.stream(dtoClass.getDeclaredFields()).filter(Field::isEnumConstant)
            .collect(Collectors.toList());

        Field randomEnum = enumFieldList.get(new Random().nextInt(enumFieldList.size()));
        return codeModel.ref(dtoClass).staticRef(randomEnum.getName());
    }


    private String generateStringParameter(Method method) {
        return method.getName().substring(3, method.getName().length());
    }


}
