package generator;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static java.beans.Introspector.decapitalize;

import javax.xml.bind.annotation.XmlType;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.ClassUtils;
import org.springframework.core.annotation.AnnotationUtils;
import org.srplib.conversion.Converter;
import org.srplib.conversion.EmptyConverter;
import org.srplib.conversion.TwoWayConverter;

import com.sun.codemodel.JBlock;
import com.sun.codemodel.JClass;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JDocComment;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JVar;
import com.ukrsibbank.client.endpoint.typemap.MtoConverter;
import com.ukrsibbank.client.protocol.product.loan.LoanMto;

import static generator.GeneratorUtils.findSetterMethods;
import static generator.GeneratorUtils.getMethodByField;
import static generator.GeneratorUtils.isStandardParameterType;
import static generator.GeneratorUtils.simpleIsName;
import static generator.GeneratorUtils.sortSetGetMethods;

/**
 * @author Q-PDV
 */
public class ConverterGenerator {

    private static final Logger LOGGER = Logger.getLogger(ConverterGenerator.class.getName());

    public static void main(String[] args) {
        JCodeModel codeModel = new JCodeModel();
        ConverterGenerator converterGenerator = new ConverterGenerator();
        try {
            converterGenerator.generateConverter(codeModel, LoanMto.class, false);
        }
        catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

    }

    public void generateConverter(JCodeModel codeModel, Class mtoClass, boolean isTwoWay) throws ClassNotFoundException {
        Class isClass;

        try {
            isClass = Class.forName("com.qulix.dbo.bankapi.bank." + simpleIsName(mtoClass));
        }
        catch (ClassNotFoundException e) {
            isClass = Class.forName("com.qulix.dbo.operation." + simpleIsName(mtoClass));
        }

        JDefinedClass generatedTestClass;

        try {

            generatedTestClass = generateClassDeclaration(codeModel, mtoClass, isClass, isTwoWay);

            generateConvertMethod(generatedTestClass, mtoClass, isClass, true, codeModel);
            if (isTwoWay) {
                generateConvertMethod(generatedTestClass, isClass, mtoClass, false, codeModel);
            }

            codeModel.build(new File("com.ukrsibbank.client.endpoint\\src\\main\\java"));

        }
        catch (JClassAlreadyExistsException | IOException e) {
            LOGGER.severe(e.getMessage());
        }

    }

    private void generateConvertMethod(JDefinedClass generatedTestClass,
                                       Class localClass,
                                       Class paramClass,
                                       boolean isDirect,
                                       JCodeModel codeModel) throws ClassNotFoundException {
        String methodName;
        if (isDirect) {
            methodName = "convert";
        } else {
            methodName = "convertBack";
        }

        JMethod convertMethod = generatedTestClass.method(JMod.PUBLIC, localClass, methodName);
        convertMethod.annotate(java.lang.Override.class);

        JVar param = convertMethod.param(paramClass, decapitalize(paramClass.getSimpleName()));

        JClass jLocalClass = codeModel.ref(localClass);

        List<Method> setters = findSetterMethods(localClass);

        List<Method> paramSetters = findSetterMethods(paramClass);

        setters = setters.stream()
            .filter(setter -> paramSetters.stream().anyMatch(paramSetter -> paramSetter.getName().equals(setter.getName())))
            .collect(Collectors.toList());

        Constructor[] constructors = localClass.getConstructors();
        boolean hasParameters = Stream.of(constructors).anyMatch(constructor -> constructor.getParameterCount() > 0);

        JVar local;

        if (setters.size() == 0 && hasParameters) {
            local = declareConstructorWithParameters(codeModel,
                localClass, paramClass, convertMethod, constructors, param, isDirect);
        } else {
            local = convertMethod.body().decl(jLocalClass, decapitalize(localClass.getSimpleName()), JExpr._new(jLocalClass));
            convertMethod.body().directStatement(" ");

            setters = sortSetGetMethods(setters, localClass);

            setParameters(setters, convertMethod.body(), local, param, paramClass, codeModel, isDirect);
        }

        convertMethod.body().directStatement(" ");

        convertMethod.body()._return(local);

    }

    private JVar declareConstructorWithParameters(JCodeModel codeModel,
                                                  Class localClass,
                                                  Class paramClass,
                                                  JMethod convertMethod,
                                                  Constructor[] constructors,
                                                  JVar param,
                                                  boolean isDirect) {
        JVar generatedStubInstance;
        Constructor constructor = Stream.of(constructors)
            .max((constructor1, constructor2) ->
                new Integer(constructor1.getParameterCount()).compareTo(constructor2.getParameterCount()))
            .get();

        JInvocation init = JExpr._new(codeModel.ref(localClass));
        List<JVar> constructParameters = new ArrayList<>();

        for (Parameter parameter : constructor.getParameters()) {
            Class constructorParameterClass = parameter.getType();

            Field paramField = getField(localClass, parameter);

            Method getMethod = getMethodByField(paramClass, paramField.getName());

            if (isStandardParameterType(constructorParameterClass) && !isHasConverter(getMethod.getReturnType(), isDirect)) {
                constructParameters.add(convertMethod.body()
                    .decl(codeModel.ref(constructorParameterClass), paramField.getName(), param.invoke(getMethod.getName())));
            } else {
                String convertMethodName;

                if (isDirect) {
                    createAdditionalConverter(constructorParameterClass, getMethod);
                    convertMethodName = "isToMb";
                } else {
                    convertMethodName = "mbToIs";
                }
                JVar attrDecl = convertMethod.body()
                    .decl(
                        codeModel.ref(constructorParameterClass),
                        paramField.getName(),
                        codeModel.ref(MtoConverter.class).staticInvoke(convertMethodName).arg(param.invoke(getMethod.getName()))
                    );

                constructParameters.add(attrDecl);
            }

        }

        convertMethod.body().directStatement(" ");

        constructParameters.forEach(init::arg);
        generatedStubInstance = convertMethod.body()
            .decl(codeModel.ref(localClass), decapitalize(localClass.getSimpleName()), init);
        return generatedStubInstance;
    }


    private Field getField(Class localClass, Parameter parameter) {
        return Stream.of(localClass.getDeclaredFields())
            .filter(field -> field.getType() == parameter.getType())
            .findFirst()
            .orElseThrow(RuntimeException::new);
    }

    private void createAdditionalConverter(Class parameterClass, Method getMethod) {
        if (!isHasConverter(getMethod.getReturnType(), true) && !isHasConverter(getMethod.getReturnType(), false)) {
            try {
                generateConverter(new JCodeModel(), parameterClass, false);
            }
            catch (ClassNotFoundException e) {
                LOGGER.severe(e.getMessage());
            }
        }
    }

    private void setParameters(List<Method> setMethods,
                               JBlock block, JVar local,
                               JVar param, Class paramClass,
                               JCodeModel codeModel,
                               boolean isDirect) {

        for (Method setMethod : setMethods) {
            Method getMethod = getMethodByField(paramClass, setMethod.getName().substring(3));

            JInvocation invocation = block.invoke(local, setMethod.getName());
            if (isStandardParameterType(setMethod) && !isHasConverter(getMethod.getReturnType(), isDirect)) {
                invocation.arg(param.invoke(getMethod.getName()));
            } else {
                if (isDirect) {
                    createAdditionalConverter(setMethod.getParameterTypes()[0], getMethod);

                    invocation.arg(codeModel.ref(MtoConverter.class).staticInvoke("isToMb")
                        .arg(param.invoke(getMethod.getName())));
                } else {
                    invocation.arg(codeModel.ref(MtoConverter.class).staticInvoke("mbToIs")
                        .arg(param.invoke(getMethod.getName())));
                }
            }
        }
    }


    private boolean isHasConverter(Class clazz, boolean isDirect) {
        if (clazz.isInterface()) {
            return false;
        }

        if (clazz.isPrimitive()) {
            clazz = ClassUtils.primitiveToWrapper(clazz);
        }

        if (isDirect) {
            return MtoConverter.getIsMbConverter(clazz) != null &&
                !EmptyConverter.class.isAssignableFrom(MtoConverter.getIsMbConverter(clazz).getClass());
        } else {
            return MtoConverter.getMbIsConverter(clazz) != null &&
                !EmptyConverter.class.isAssignableFrom(MtoConverter.getMbIsConverter(clazz).getClass());
        }
    }

    private JDefinedClass generateClassDeclaration(JCodeModel codeModel,
                                                   Class mtoClass,
                                                   Class isClass,
                                                   boolean isTwoWay) throws JClassAlreadyExistsException {
        JDefinedClass generatedTestClass = codeModel.
            _class("com.ukrsibbank.client.endpoint.typemap.converters.Mto" + simpleIsName(mtoClass) + "Converter");

        if (isTwoWay) {
            generatedTestClass._implements(codeModel.ref(TwoWayConverter.class).narrow(isClass, mtoClass));
        } else {
            generatedTestClass._implements(codeModel.ref(Converter.class).narrow(isClass, mtoClass));
        }

        JDocComment comment = generatedTestClass.javadoc()
            .append("Конвертирует объекты  ")
            .append(codeModel.ref(isClass)).append(" в ")
            .append(codeModel.ref(mtoClass));

        if (isTwoWay) {
            comment.append("и обратно");
        }
        comment.add("\n\n@author Q-PDV");
        return generatedTestClass;
    }

}
