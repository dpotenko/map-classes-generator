package com.qulix.dbo;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Assert;

import com.qulix.dbo.bankapi.bank.NamedAmount;
import com.qulix.dbo.bankapi.bank.NotificationEvent;
import com.qulix.dbo.bankapi.bank.OperationTemplateAttributes;
import com.qulix.dbo.bankapi.bank.ProductIdentity;
import com.qulix.dbo.bankapi.bank.StandingOrderAttributes;
import com.qulix.dbo.bankapi.bank.TariffNode;
import com.qulix.dbo.bankapi.bank.TariffPlan;
import com.qulix.dbo.bankapi.bank.Transaction;
import com.qulix.dbo.bankapi.bank.TransactionAttributes;
import com.qulix.dbo.bankapi.bank.TransactionCategory;
import com.qulix.dbo.bankapi.bank.TransactionParticipant;
import com.qulix.dbo.bankapi.bank.TransactionQueryResult;
import com.qulix.dbo.bankapi.bank.TransactionalOperationResult;
import com.qulix.dbo.bankapi.bank.item.AccountItem;
import com.qulix.dbo.bankapi.bank.item.CardItem;
import com.qulix.dbo.bankapi.bank.item.DepositItem;
import com.qulix.dbo.bankapi.bank.item.LoanItem;
import com.qulix.dbo.bankwsapi.sei.AccountItemDto;
import com.qulix.dbo.bankwsapi.sei.CardItemDto;
import com.qulix.dbo.bankwsapi.sei.DepositItemDto;
import com.qulix.dbo.bankwsapi.sei.LoanItemDto;
import com.qulix.dbo.bankwsapi.sei.NamedAmountDto;
import com.qulix.dbo.bankwsapi.sei.NotificationEventDto;
import com.qulix.dbo.bankwsapi.sei.OperationTemplateAttributesDto;
import com.qulix.dbo.bankwsapi.sei.ProductIdentityDto;
import com.qulix.dbo.bankwsapi.sei.StandingOrderAttributesDto;
import com.qulix.dbo.bankwsapi.sei.TariffNodeDto;
import com.qulix.dbo.bankwsapi.sei.TariffPlanDto;
import com.qulix.dbo.bankwsapi.sei.TransactionAttributesDto;
import com.qulix.dbo.bankwsapi.sei.TransactionParticipantDto;
import com.qulix.dbo.bankwsapi.sei.TransactionQueryResultDto;
import com.qulix.dbo.bankwsapi.sei.TransactionalOperationResultDto;
import com.qulix.dbo.typemap.DtoConverter;
import com.qulix.dbo.typemap.converters.DtoStubGenerator;
import com.sun.codemodel.JClass;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JDocComment;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JVar;

/**
 * @author PotenkoDV
 */
public class TestGenerator extends Generator {

    private JCodeModel codeModel = new JCodeModel();
    private MatcherGenerator matcherGenerator = new MatcherGenerator();

    public static void main(String[] args) throws JClassAlreadyExistsException, IOException, ClassNotFoundException {
        FabricGenerator fabricGenerator = new FabricGenerator();
        fabricGenerator.generateCreateClassMethod(LoanItemDto.class);
        TestGenerator testGenerator = new TestGenerator();
        testGenerator.generateTestClass(LoanItem.class, true);

       /* MatcherGenerator matcherGenerator = new MatcherGenerator();
        matcherGenerator.generateMatcher(Transaction.class);*/
    }


    public void generateTestClass(Class isClass, boolean isTwoWay) throws IOException, JClassAlreadyExistsException, ClassNotFoundException {
        Class dtoClass = Class.forName("com.qulix.dbo.bankwsapi.sei." + isClass.getSimpleName() + "Dto");
        JDefinedClass generatedTestClass = generateClassDeclaration(isClass, dtoClass, isTwoWay);
        generateTestMethod(generatedTestClass, dtoClass, isClass, true);
        if (isTwoWay) {
            generateTestMethod(generatedTestClass, dtoClass, isClass, false);
        }
        codeModel.build(new File("com.qulix.dbo.bankwsapi\\src\\test\\java"));
    }

    private JDefinedClass generateClassDeclaration(Class isClass, Class dtoClass, boolean isTwoWay) throws JClassAlreadyExistsException {
        JDefinedClass generatedTestClass = codeModel._class("com.qulix.dbo.typemap.converters.Dto" + isClass.getSimpleName() + "ConverterTest");
        JDocComment comment = generatedTestClass.javadoc().append("Тесты для конвертера ").append(codeModel.ref(dtoClass)).append(" в ").append(codeModel.ref(isClass));
        if (isTwoWay) {
            comment.append("и обратно");
        }
        comment.add("\n\n@author Q-PDV");
        return generatedTestClass;
    }

    private JVar initStubInstance(JMethod testMethod, Class generatedStubClass, JExpression initExpression, boolean isDto) {
        JClass jDtoClass = codeModel.ref(generatedStubClass);
        return testMethod.body().decl(jDtoClass, isDto ? "dtoObj" : "isObj", initExpression);
    }

    private void generateTestMethod(JDefinedClass generatedTestClass, Class dtoClass, Class isClass, boolean isDirect) throws JClassAlreadyExistsException, IOException, ClassNotFoundException {
        String methodName;
        if (isDirect) {
            methodName = "testConvert";
        } else {
            methodName = "testConvertBack";
        }
        JMethod testMethod = generatedTestClass.method(JMod.PUBLIC, void.class, methodName);
        testMethod.annotate(org.junit.Test.class);

        JClass stubGenerator = codeModel.ref(DtoStubGenerator.class);
        JClass dtoConverter = codeModel.ref(DtoConverter.class);

        JVar generatedStubInstanceIs;
        JVar generatedStubInstanceDto;
        if (isDirect) {
            generatedStubInstanceDto = initStubInstance(testMethod, dtoClass, stubGenerator.staticInvoke("create" + isClass.getSimpleName()), true);
            generatedStubInstanceIs = initStubInstance(testMethod, isClass, dtoConverter.staticInvoke("dtoToIs").arg(generatedStubInstanceDto), false);
        } else {
            generatedStubInstanceIs = initStubInstance(testMethod, isClass, dtoConverter.staticInvoke("dtoToIs").arg(stubGenerator.staticInvoke("create" + isClass.getSimpleName())), false);
            generatedStubInstanceDto = initStubInstance(testMethod, dtoClass, dtoConverter.staticInvoke("isToDto").arg(generatedStubInstanceIs), true);
        }

        testMethod.body().directStatement(" ");

        JClass jAssert = codeModel.ref(Assert.class);

        List<Method> methods = findGetterMethods(isClass);

        JClass jMatcher = codeModel.ref(Matchers.class);
        for (Method getMethod : methods) {
           /* if (!isMethodExists(dtoClass, getMethod)) {
                continue;
            }*/
            JInvocation assertInvoke = testMethod.body().staticInvoke(jAssert, "assertThat");
            JInvocation getterInvokeIs = createGetterInvoke(getMethod, generatedStubInstanceIs);
            JInvocation getterInvokeDto = createGetterInvoke(getMethod, generatedStubInstanceDto);
            assertInvoke.arg(getterInvokeIs);

            if (isStandardReturnType(getMethod)) {
                assertInvoke.arg(jMatcher.staticInvoke("is").arg(getterInvokeDto));
            } else {
                JClass matcher = matcherGenerator.findMatcher(getMethod.getReturnType(), codeModel);
                assertInvoke.arg(matcher.staticInvoke("same" + getMethod.getReturnType().getSimpleName()).arg(getterInvokeDto));
            }
        }
    }


}
