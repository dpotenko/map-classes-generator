package com.qulix.dbo;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import static java.beans.Introspector.decapitalize;

import com.sun.codemodel.JBlock;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JFieldRef;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JVar;

/**
 * @author PotenkoDV
 */
public class FabricGenerator extends Generator {

    private JCodeModel codeModel;

    private JDefinedClass fabricClass;

    public void generateCreateClassMethod(Class dtoClass) throws JClassAlreadyExistsException, IOException {
        codeModel = new JCodeModel();
        fabricClass = codeModel._class("com.qulix.dbo.Fabric");
        recursiveGenerateCreateClassMethod(codeModel, dtoClass);
        codeModel.build(new File("com.qulix.dbo.bankwsapi\\src\\test\\java"));
    }

    private JExpression recursiveGenerateCreateClassMethod(JCodeModel codeModel, Class dtoClass) {

        if (dtoClass.isEnum()) {
            return getEnumFieldRef(dtoClass);
        }

        JMethod createMethod = fabricClass.method(JMod.PUBLIC + JMod.STATIC, dtoClass, "create" + createIsClassNameFromDto(dtoClass));

        JVar generatedStubInstance = createMethod.body()
            .decl(codeModel.ref(dtoClass), decapitalize(dtoClass.getSimpleName()), JExpr._new(codeModel.ref(dtoClass)));

        List<Method> setMethods = findSetterMethods(dtoClass);

        setStubParameters(setMethods, createMethod.body(), generatedStubInstance);

        createMethod.body()._return(generatedStubInstance);
        return JExpr.invoke(createMethod);
    }

    private void setStubParameters(List<Method> setMethods, JBlock block, JVar generatedStubInstance) {
        for (Method setMethod : setMethods) {
            if (isStandardParameterType(setMethod)) {
                createPrimitiveInstance(block, setMethod, generatedStubInstance, setMethod.getParameterTypes()[0].getSimpleName());
            } else {
                block.invoke(generatedStubInstance, setMethod.getName()).arg(recursiveGenerateCreateClassMethod(codeModel, setMethod.getParameterTypes()[0]));
            }
        }
    }

    private void createPrimitiveInstance(JBlock block, Method setMethod, JVar generatedStubInstance, String simpleName) {
        JInvocation invocation = block.invoke(generatedStubInstance, setMethod.getName());
        if (simpleName.equals("String")) {
            invocation.arg("test" + generateStringParameter(setMethod));
        }
        if (simpleName.equals("Date")) {
            invocation.arg(JExpr._new(codeModel.ref(Date.class)));
        }
        if (simpleName.equals("BigDecimal")) {
            invocation.arg(JExpr._new(codeModel.ref(BigDecimal.class)).arg(JExpr.lit(ThreadLocalRandom.current().nextInt(0, 1000))));
        }
        if (simpleName.equals("Boolean") || simpleName.equals("boolean")) {
            invocation.arg(JExpr.lit(new Random().nextBoolean()));
        }
        if (simpleName.equals("int")) {
            invocation.arg(JExpr.lit(ThreadLocalRandom.current().nextInt(0, 1000)));
        }
    }

    private JFieldRef getEnumFieldRef(Class dtoClass) {
        List<Field> enumFieldList = Arrays.stream(dtoClass.getDeclaredFields()).filter(Field::isEnumConstant).collect(Collectors.toList());
        Field randomEnum = enumFieldList.get(new Random().nextInt(enumFieldList.size()));
        return codeModel.ref(dtoClass).staticRef(randomEnum.getName());
    }


    private String generateStringParameter(Method method) {
        return method.getName().substring(3, method.getName().length());
    }

    private String createIsClassNameFromDto(Class dtoClass) {
        return dtoClass.getSimpleName().substring(0, dtoClass.getSimpleName().length() - 3);
    }

}
